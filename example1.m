%% ===================================================================== %%
%  Academic example used in the 2021 LPVS paper:
%    C. Verhoek, H. S. Abbas, R. Tóth, and S. Haesaert. "Data-Driven
%    Predictive Control for Linear Parameter-Varying Systems." In Proc. of
%    the 4th IFAC Workshop on Linear Parameter-Varying Systems (LPVS), 2021.
%
%  The code compares a DPC and MPC on an externally scheduled LPV system
%
%  Authors: H. S. Abbas and C. Verhoek
%
%  Released under the BSD 3-Clause License. 
%  Copyright (c) 2022, Eindhoven, The Netherlands
%
%
%  Final version -- Results used in published paper.
%  Tested in ML2020b, ML2021b on both mac and windows machine
%% ===================================================================== %%
clc; clear variables; close all;
rng(350);
plot_results = true;
%% This script compares data-driven lpv predictive controller
% (using certain Hankel matrix) and classic lpv mpc based on an lpvio model


%%  Setting for the predictive controller

nd = 100;

% tuning parameters
Np = 5; % prediction horizon
Q = 1;
R = 1e-2;

% constraints
ymax =  1;
ymin = -1;
umax =  5;
umin = -5;

% optimiztion and simulation time
ksim = 30+Np;


% constructing reference signal
t = (1:1:ksim)';
r(t<=7) = 0.5;
r(t>7 & t<=11) = -0.5;
r(t>11 & t<=13) = -0.5;
r(t>13 & t<=19) = 0.7;
r(t>19 & t<=24) = -0.7;
r(t>24 & t<=25) = 0.8;
r(t>25) = 0.8;
r = r';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% generating LPV data

% model parameters
Param = [1    -0.5  -0.1;
    0.5  -0.7  -0.1;
    0.5  -0.4   0.01;
    0.2  -0.3  -0.02];


y0  = 0;

u   = 1-2*rand(nd,1);
p1  = 0.5*sin(0.35*pi*(1:nd))'+0.5;
p2  = p1.^2;

%  lpvio model simulation
y = sim_lpv_io(Param, nd, y0, u, p1, p2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% data-driven lpv predictive control
% Arranging data
p = [p1 p2];

nl = 2; % the lag of the system
n  = 2; % minimal order of the system
nh  = nl+Np;
nu  = size(u,2);
ny  = size(y,2);
np  = size(p,2);
pnu = np*nu;
pny = np*ny;
ns  = pnu+nu+pny+ny;
Nd  = (ns+1)*nh-1;
i0  = 1;
id  = Nd+nl+1+i0;

% create Hankel matrices
[Hnlu, Hnlup, Hnly, Hnlyp, HLu, HLup, HLy, HLyp] = hanks2(u(i0:id-nl-1,:), y(i0:id-nl-1,:), p(i0:id-nl-1,:), n, nl, Np);


%% ------- Design and implmentation of the proposed lpvdpc

% fixed matrices
zupini = zeros(np*nl, 1);
zypini = zeros(np*nl, 1);
zuph   = zeros(np*Np, 1);
zyph   = zeros(np*Np, 1);

% Settings of the solver
options = sdpsettings('solver', 'gurobi', 'verbose', 0);
g       = sdpvar(size(HLu,2), 1);


% inititalizing the lpvdpc
y0   = 0;
ydpc = zeros(ksim-Np, 1);
udpc = zeros(ksim-Np, 1);
J    = zeros(ksim-Np, 1);
uini = zeros(nu*nl,1);
yini = [0; y0];
pini = [p(1:1,:);p(1:1,:)];
ydpc(1) = y0;

% LPV DPC simulation
disp('LPV DPC steps:')
for k = 2:ksim-Np
    
    fprintf('%.f\n',k);
    
    [coluini, colyini, dpini] = coldiag(uini, yini, pini);
    
    ph = p(k:k+Np-1,:);
    
    %  cost function and constraints
    uh = HLu*g;
    yh = HLy*g;
    [coluh, ~, dph] = coldiag(uh, [], ph);
    
    X = [Hnlu
        Hnlup - dpini*Hnlu
        Hnly
        Hnlyp - dpini*Hnly
        HLu
        HLup - dph*HLu
        HLyp - dph*HLy
        ];
    h =[coluini; zupini; colyini; zypini; coluh; zuph; zyph];
    
    Constraints = X*g == h;
    Objective   = 0;
    for ik = 0:Np-1
        Objective   = Objective + (yh(ik+1)-r(k+ik))'*Q*(yh(ik+1)-r(k+ik)) + uh(ik+1)'*R*uh(ik+1);
        Constraints = [Constraints,  umin <= uh(ik+1) <= umax];%#ok
        Constraints = [Constraints,  ymin <= yh(ik+1) <= ymax];%#ok
    end
    
    % QP optimization
    diagnostics = optimize(Constraints, Objective ,options);
    opttime = diagnostics.solvertime;
    if diagnostics.problem ~= 0
        error('infeasible initial condition check the domain of attraction');
    end
    J(k) = double(Objective);
    gk    = double(g);
    
    % computing control input
    udpc(k) = double(uh(1));
    
    % implementation onto the plant
    ydpc(k) = lpv_io_stepk(Param, k, udpc, p1, p2, ydpc);
    
    % initializing the lpvdpc for the next iteration
    uini  = [uini(2:end); udpc(k)];
    yini  = [yini(2:end); ydpc(k)];
    pini =  [pini(2:end,:);p(k,:)];
    
end

tk = (1:Nd)';


%% plots
% plot settings
fontsz_label = 16;
fontsz_tick = 12;
fontsz_leg = 12;
linewdt = 2;

% colors
dgreen = [0 75 90]/255;
green   = [0, 0, 128]/255;
blue  = [34,139,34]/255;
dred    = [181 22 33]/255;
red   = [255 22 33]/255;
yellow = [237,192,1]/255;
gray   = [1 1 1]*0.75;
black   = [1 1 1]*0;

if plot_results
    % plot 1
    f1 = figure(1);clf; f1.Position(3:4) = [800 150];
    subplot(1,3,1); hold on; grid on; box on; set(gca,'fontsize',fontsz_tick)
    stairs(tk(1:Nd),u(1:Nd),'Color',dgreen, 'LineWidth',linewdt)
    axis([0 tk(Nd) -1.2 1.2]);
    ylabel('$u(k)$','Interpreter','latex','fontsize',fontsz_label)
    xlabel('Sample $k$','Interpreter','latex','fontsize',fontsz_label)
    f1ax1 = gca;
    
    subplot(1,3,2); hold on; grid on; box on; set(gca,'fontsize',fontsz_tick)
    stairs(tk(1:Nd),p(1:Nd,1),'Color',dgreen, 'LineWidth',linewdt)
    axis([0 tk(Nd) -0.2 1.2*max(p(1:Nd))]);
    ylabel('$p(k)$','Interpreter','latex','fontsize',fontsz_label)
    xlabel('Sample $k$','Interpreter','latex','fontsize',fontsz_label)
    f1ax2 = gca;
    
    subplot(1,3,3); hold on; grid on; box on;set(gca,'fontsize',fontsz_tick)
    stairs(tk(1:Nd),y(1:Nd),'Color',dgreen, 'LineWidth',linewdt)
    ylabel('$y(k)$','Interpreter','latex','fontsize',fontsz_label)
    xlabel('Sample $k$','Interpreter','latex','fontsize',fontsz_label)
    dify = 0.05*(max(y(1:Nd))-min(y(1:Nd)));
    axis([0 tk(Nd) min(y(1:Nd))-dify, dify+max(y(1:Nd))]);
    f1ax3 = gca;
    
    
    
    
    %% plot 2
    f2 = figure(2);clf; f2.Position(3:4) = [600 300];
    % subplot 1
    subplot(2,2,1);hold on; grid on; box on; set(gca,'fontsize',fontsz_tick)
    plot(tk(1:k), 5*ones(k,1),'Color',gray, 'LineWidth',linewdt, 'HandleVisibility','off')
    plot(tk(1:k),-5*ones(k,1),'Color',gray, 'LineWidth',linewdt, 'HandleVisibility','off')
    stairs(tk(1:k),udpc(1:k),'Color',dgreen, 'LineWidth',linewdt, 'DisplayName', '$u_{\mathrm{DPC}}$');
    ylabel('$u(k)$','Interpreter','latex','fontsize',fontsz_label)
    xlabel('Sample $k$','Interpreter','latex','fontsize',fontsz_label)
    axis([0 tk(k) -6 6]);
    f2ax1 = gca;
    
    % subplot 2
    subplot(2,2,2);hold on; grid on; box on; set(gca,'fontsize',fontsz_tick)
    stairs(tk(1:k),p(1:k,1),'Color',dgreen, 'LineWidth',linewdt, 'DisplayName', '$p_{\mathrm{DPC}}$');
    ylabel('$p(k)$','Interpreter','latex','fontsize',fontsz_label)
    xlabel('Sample $k$','Interpreter','latex','fontsize',fontsz_label)
    axis([0 tk(k) -0.2 1.2*max(p(1:k,1))]);
    f2ax2 = gca;
    
    % subplot 3, big
    subplot(2,2,[3,4]);hold on; grid on; box on; set(gca,'fontsize',fontsz_tick)
    stairs(tk(1:k),ones(k,1),'Color',gray, 'LineWidth',linewdt, 'HandleVisibility','off')
    plot(tk(1:k),-ones(k,1),'Color',gray, 'LineWidth',linewdt, 'HandleVisibility','off')
    stairs(tk(1:k),r(1:k),'Color',yellow, 'LineWidth',linewdt, 'DisplayName', '$r(k)$')
    stairs(tk(1:k),ydpc(1:k),'Color',dgreen, 'LineWidth',linewdt, 'DisplayName', '$y_{\mathrm{DPC}}$')
    ylabel('$r(k)$ and $y(k)$','Interpreter','latex','fontsize',fontsz_label)
    xlabel('Sample $k$','Interpreter','latex','fontsize',fontsz_label)
    axis([0 tk(k) -1.2 1.2]);
    f2ax3 = gca;
end

%% CLASSIC LPV MPC
clear yalmip

% unpack params
a1  = Param(1,1);
a2  = Param(2,1);
b11 = Param(3,1);
b12 = Param(4,1);
b21 = Param(3,2);
b22 = Param(4,2);
b31 = Param(3,3);
b32 = Param(4,3);
b41 = Param(1,2);
b42 = Param(2,2);
b51 = Param(1,3);
b52 = Param(2,3);

% init params
uik  = sdpvar(Np,1);

ympc    = zeros(ksim-Np, 1);
umpc    = zeros(ksim-Np, 1);
J       = zeros(ksim-Np, 1);
ympc(1) = y0;

disp('Classic LPV MPC steps:')
for k = 2:ksim-Np
    
    fprintf('%.f\n',k);
    % implementation onto the plant
    ympc(k) = lpv_io_stepk(Param, k, umpc, p1, p2, ympc);
    
    %  cost function and constraints
    Objective = uik(1)'*R*uik(1);
    yik_1  = ympc(k-1);
    yik_0  = ympc(k);
    a1p  = a1+b41*p1(k)+b51*p2(k);
    a2p  = a2+b42*p1(k-1)+b52*p2(k-1);
    b1p  = b11+b21*p1(k)+b31*p2(k);
    b2p  = b12+b22*p1(k-1)+b32*p2(k-1);
    yik    = -a1p*yik_0 - a2p*yik_1  + b1p*uik(1) + b2p*umpc(k-1);
    Constraints = umin <= uik(1) <= umax;%#ok
    for ik = 1:Np-1
        Objective   = Objective + (yik-r(k+ik))'*Q*(yik-r(k+ik)) + uik(ik+1)'*R*uik(ik+1);
        Constraints = [Constraints,  umin <= uik(ik+1) <= umax ];%#ok
        Constraints = [Constraints,  ymin <= yik <= ymax];%#ok
        yik_1  = yik_0;
        yik_0  = yik;
        a1p  = a1+b41*p1(k+ik)+b51*p2(k+ik);
        a2p  = a2+b42*p1(k+ik-1)+b52*p2(k+ik-1);
        b1p  = b11+b21*p1(k+ik)+b31*p2(k+ik);
        b2p  = b12+b22*p1(k+ik-1)+b32*p2(k+ik-1);
        yik  = -a1p*yik_0 - a2p*yik_1  + b1p*uik(ik+1) + b2p*uik(ik);
    end
    Constraints = [Constraints,  ymin <= yik <= ymax];%#ok
    
    
    % QP optimization
    diagnostics = optimize(Constraints, Objective ,options);
    opttime = diagnostics.solvertime;
    if diagnostics.problem ~= 0
        error('infeasible initial condition check the domain of attraction');
    end
    J(k) = double(Objective);
    U    = double(uik);
    
    % computing control input
    umpc(k) = double(U(1));
    
end

if plot_results
    stairs(f2ax1,tk(1:k),umpc(1:k),'r--','DisplayName','$u_{\mathrm{MPC}}$', 'LineWidth',linewdt)
    stairs(f2ax2, tk(1:k),p(1:k,1),'r--', 'DisplayName', '$p_{\mathrm{MPC}}$', 'LineWidth',linewdt);
    stairs(f2ax3, tk(1:k),ympc(1:k),'r--','DisplayName','$y_{\mathrm{MPC}}$', 'LineWidth',linewdt)
    %% legend
    l1 = legend(f2ax1,'show','fontsize',fontsz_leg);
    l2 = legend(f2ax2,'show','fontsize',fontsz_leg);
    l3 = legend(f2ax3,'show','fontsize',fontsz_leg);
end





%% ########################################################################
%  #######                                                          #######
%  #######                     LOCAL FUNCTIONS                      #######
%  #######                                                          #######
%  ########################################################################
function y = sim_lpv_io(Param, nd, y0, u, p1, p2)
y    = zeros(nd,1);
y(1) = y0;
for k =1:nd
    y(k) = lpv_io_stepk(Param, k, u, p1, p2, y);
end
end

function yk = lpv_io_stepk(Param, k, u, p1, p2, y)
a1  = Param(1,1);
a2  = Param(2,1);
b11 = Param(3,1);
b12 = Param(4,1);
b21 = Param(3,2);
b22 = Param(4,2);
b31 = Param(3,3);
b32 = Param(4,3);
b41 = Param(1,2);
b42 = Param(2,2);
b51 = Param(1,3);
b52 = Param(2,3);

if k == 1
    yk = y(1);
elseif k == 2
    a1p = a1+b41*p1(k-1)+b51*p2(k-1);
    b1p = b11+b21*p1(k-1)+b31*p2(k-1);
    yk = -a1p*y(k-1)+b1p*u(k-1);
else
    a1p = a1+b41*p1(k-1)+b51*p2(k-1);
    a2p = a2+b42*p1(k-2)+b52*p2(k-2);
    b1p = b11+b21*p1(k-1)+b31*p2(k-1);
    b2p = b12+b22*p1(k-2)+b32*p2(k-2);
    yk  = -a1p*y(k-1)-a2p*y(k-2)+b1p*u(k-1)+b2p*u(k-2);
end

end

function [Hnlu, Hnlup, Hnly, Hnlyp, HLu, HLup, HLy, HLyp] = hanks2(varargin)
% This function constructs the required Hankel matrices for data-
% driven lpv simulation or predictive control.
% It is based on the new formulation in our LPVS paper
%
% Give the full input/output/schduling u/y/p data as column versotrs
% For signals multivariate signals, they are given as matrices as
% s=[s1 s2 ...]
% n: order of the system
% nl: the lag of the system, which is related to the l-step
%     observabaility matrix Q_l, l that enables us to determine x(0) or
%     that makes Q_l has rank n
% L  %%<<<========= Specify here the number of k-step a head for
%                   simulation or prediction horizon of predictive control
%
% H. S. Abbas
% Lübeck 11.02.2021
%
% NOTE: Stripped down version...


%% Input parsing
u  = varargin{1};
y  = varargin{2};
p  = varargin{3};
%n  = varargin{4};
nl = varargin{5};
L  = varargin{6};
N = size(u,1);

%% constructing io signals with scheduling parameter (only works for ny=nu=1)
up = p.*u;
yp = p.*y;

%% Hankel matrices
Hnlu = hank2(u(1:N-L,:), nl);
Hnlup = hank2(up(1:N-L,:), nl);
Hnly = hank2(y(1:N-L,:), nl);
Hnlyp = hank2(yp(1:N-L,:), nl);
HLu = hank2(u(nl+1:N,:), L);
HLup = hank2(up(nl+1:N,:), L);
HLy = hank2(y(nl+1:N,:), L);
HLyp = hank2(yp(nl+1:N,:), L);
end

function H = hank2(u, L)
% This function constructs the Hankel matrix for u
% u: is a colum vector or matrix consists of colum vectors of signals
% L: No. row blocks in the resulted Hankel matrix
% The update of the function hank where now we use matlab function 'hankel'
%
% HSA
% Lübeck 11.2.2021

u = u';
[m, n] = size(u);
i = hankel(1:n);
i = i(1:L, 1:n-L+1);
H = reshape(u(:,i), m*L, []);
end

function [colu, coly, diagp] = coldiag(u, y, p)
% Constructing columns from u, y input/output, respc. and a block diagonal
% matrix from p
% Multivariate signals should be as s=[s1 s2 ...], si are column vectors
%
% HSA
% Lübeck 14.02.2021


%% setting up dimensions
[N, nu]  = size(u);
if ~isempty(y)
    ny  = size(y,2);
end



%% the column signals correspnding to the initial conditions uini, yini, pini
colu   = reshape(u',[nu*N,1]);
if ~isempty(y)
    coly   = reshape(y',[ny*N,1]);
else
    coly   = [];
end
p  = p';
Cp = mat2cell(p, size(p,1), ones(1,size(p,2)));    %// step 1
diagp = blkdiag(Cp{:});
end

